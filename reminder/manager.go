package reminder

type Reminder struct {
	Text string
}

type Map interface {
	Add(date string, reminder Reminder)
	Clear(date string)
	GetElements() map[string][]Reminder
}

type Manager struct {
	Reminders Map
}

func (m *Manager) Add(date string, reminder Reminder) {
	m.Reminders.Add(date, reminder)
	return
}

func (m *Manager) Clear(date string) {
	m.Reminders.Clear(date)
	return
}

func (m Manager) GetReminders() map[string][]Reminder {
	return m.Reminders.GetElements()
}
